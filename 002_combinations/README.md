# combinations

Finds the number of k-combinations in a set of n elements. Powered by `big`.

Usage:

```sh
go run findmax N K

# N - set size
# K - sample size
```
