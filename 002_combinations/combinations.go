package main

import (
	"fmt"
	"math/big"
	"os"
	"strconv"
)

func main() {
	var n, k int64

	if len(os.Args) > 2 {
		nArg, err := strconv.Atoi(os.Args[1])

		if err != nil {
			fmt.Println(err)
			return
		}

		n = int64(nArg)

		kArg, err := strconv.Atoi(os.Args[2])

		if err != nil {
			fmt.Println(err)
			return
		}

		k = int64(kArg)
	} else {
		return
	}

	fmt.Println(findCombinations(n, k))
}

func findCombinations(n, k int64) *big.Int {
	result := big.NewInt(1)

	if k > n-k {
		k = n - k
	}

	for j := int64(1); j <= k; j++ {
		if n%j == 0 {
			result.Mul(result, big.NewInt(n/j)) // result *= n/j
		} else if result.Int64()%j == 0 {
			result.Div(result, big.NewInt(j)) // result = result/j * n
			result.Mul(result, big.NewInt(n))
		} else {
			result.Mul(result, big.NewInt(n)) // result = (result*n) / j
			result.Div(result, big.NewInt(j))
		}

		n--
	}

	return result
}
