# con-math

Solutions to Concrete Mathematics problems, given in Concrete Mathematics
course at HS ITIS, KFU.

All (or most) of the solutions are written in Go since it outperforms Java and
Python and it's friendlier than C/C++ or Rust.
