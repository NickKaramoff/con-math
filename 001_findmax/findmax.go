package main

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"
)

const n int = 100000000

func main() {
	var iters int

	if len(os.Args) > 1 {
		iterArg, err := strconv.Atoi(os.Args[1])

		if err != nil {
			fmt.Println(err)
			return
		}

		iters = iterArg
	} else {
		iters = 1
	}

	for j := 0; j < iters; j++ {
		countAssignments()
	}
}

func countAssignments() {
	rand.Seed(time.Now().UnixNano())

	var array [n]float64

	for i := 0; i < n; i++ {
		array[i] = rand.Float64()
	}

	var max float64 = array[0]
	var counter int = 1

	for i := 1; i < n; i++ {
		if array[i] > max {
			counter++
			max = array[i]
		}
	}

	fmt.Println("Присваиваний:", counter)
}
